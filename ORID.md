## Objective

- Today, the principles of task splitting and splitting are learned. Also, how to realize the needs of users must first clear the needs, then split the task, and then make the Context Map according to the sub -task. After obtaining the Context Map It will be very easy to encode again. And when I learned Git Commit, there were various types of Message, and the Commit needed a small step to submit. This can have positive feedback and achieve a sense of accomplishment.

## Reflective

- The difficult problem encountered is the focus of today's course, that is, task disassembly and Context Map.

## Interpretive

- Before the non -contact task decomposition and Context Map, I was coded directly according to the needs, but this method can only cope with some simple needs. After today's course, I felt the role of solving complex needs.

## Decision

- In the future, when encountering demand, I can use the methods learned today to perform a standardized analysis, and then coded.
