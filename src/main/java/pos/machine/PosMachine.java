package pos.machine;

import java.util.*;

public class PosMachine {
    public String printReceipt(List<String> barcodes) {
        List<ReceiptItem> receiptItems = decodeToItems(barcodes);
        Receipt receipt = calculateCost(receiptItems);
        return renderReceipt(receipt);
    }

    public String renderReceipt(Receipt receipt) {
        String itemsReceiptStr = generateItemsReceipt(receipt);
        return generateReceipt(itemsReceiptStr, receipt.getTotal());
    }

    public String generateReceipt(String itemsReceipt, int total) {
        StringJoiner stringJoiner = new StringJoiner("\n");
        stringJoiner.add("***<store earning no money>Receipt***");
        stringJoiner.add(itemsReceipt);
        stringJoiner.add("----------------------");
        stringJoiner.add(String.format("Total: %d (yuan)", total));
        stringJoiner.add("**********************");
        return stringJoiner.toString();
    }


    public String generateItemsReceipt(Receipt receipt) {
        StringJoiner stringJoiner = new StringJoiner("\n");
        for (ReceiptItem receiptItem : receipt.getReceiptItems()) {
            String formatStringItem = String.format("Name: %s, Quantity: %d, Unit price: %d (yuan), Subtotal: %d (yuan)",
                    receiptItem.getName(), receiptItem.getQuantity(), receiptItem.getUnitPrice(), receiptItem.getSubtotal());
            stringJoiner.add(formatStringItem);
        }
        return stringJoiner.toString();
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems) {
        Receipt receipt = new Receipt();
        List<ReceiptItem> receiptItems1 = calculateItemsCost(receiptItems);
        int totalPrice = calculateTotalPrice(receiptItems);
        receipt.setReceiptItems(receiptItems1);
        receipt.setTotal(totalPrice);
        return receipt;
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItems) {
        int total = 0;
        for (ReceiptItem receiptItem : receiptItems) {
            total += receiptItem.getSubtotal();
        }
        return total;
    }

    public List<ReceiptItem> calculateItemsCost(List<ReceiptItem> receiptItems) {
        for (ReceiptItem receiptItem : receiptItems) {
            receiptItem.setSubtotal(receiptItem.getUnitPrice() * receiptItem.getQuantity());
        }
        return receiptItems;
    }

    public List<ReceiptItem> decodeToItems(List<String> barcodes) {

        List<Item> items = loadAllItems();
        Map<String, Integer> barcodeQuantityMap = new LinkedHashMap<>();

        for (String barcode : barcodes) {
            for (Item item : items) {
                if (barcode.equals(item.getBarcode())) {
                    if (barcodeQuantityMap.containsKey(item.getBarcode())) {
                        barcodeQuantityMap.put(item.getBarcode(), barcodeQuantityMap.get(item.getBarcode()) + 1);
                    } else {
                        barcodeQuantityMap.put(item.getBarcode(), 1);
                    }
                }
            }
        }

        List<ReceiptItem> receiptItems = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : barcodeQuantityMap.entrySet()) {
            ReceiptItem receiptItem = new ReceiptItem();
//            receiptItem.setName(entry.getKey());
            receiptItem.setQuantity(entry.getValue());
            for (Item item : items) {
                if (entry.getKey().equals(item.getBarcode())) {
                    receiptItem.setName(item.getName());
                    receiptItem.setUnitPrice(item.getPrice());
//                    receiptItem.setSubtotal(entry.getValue() * item.getPrice());
                }
            }
            receiptItems.add(receiptItem);
        }
        return receiptItems;
    }

    public List<Item> loadAllItems() {
        return ItemsLoader.loadAllItems();
    }

}
