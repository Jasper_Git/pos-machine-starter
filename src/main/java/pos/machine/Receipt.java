package pos.machine;

import java.util.List;

/**
 * @program: pos-machine-starter
 * @author: yoki
 * @create: 2023-07-11 20:33
 */
public class Receipt {
    private List<ReceiptItem> receiptItems;
    private Integer total;

    public Receipt() {
    }

    public Receipt(List<ReceiptItem> receiptItems, Integer total) {
        this.receiptItems = receiptItems;
        this.total = total;
    }

    public List<ReceiptItem> getReceiptItems() {
        return receiptItems;
    }

    public void setReceiptItems(List<ReceiptItem> receiptItems) {
        this.receiptItems = receiptItems;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}

